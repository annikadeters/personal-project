const homePage = document.querySelector("#dashboard");
const createPage = document.querySelector("#create");

const postTitle = document.querySelector("#post__title");
const postText = document.querySelector("#post__text");

const homeButton = document.querySelector("#home__button");
const createButton = document.querySelector("#create__button");
const postButton = document.querySelector("#post__button");

homeButton.addEventListener("click", homeSwitch);
createButton.addEventListener("click", createSwitch);

function homeSwitch() {
  createPage.classList.add("hidden");
  homePage.classList.remove("hidden");
}

function createSwitch() {
  homePage.classList.add("hidden");
  createPage.classList.remove("hidden");
}

postButton = addEventListener("click", postGathering);

function postGathering() {
  homePage.insertAdjacentHTML(
    "beforeend",
    "<div class='dashboard__post'>" +
      postTitle.value +
      "</div>" +
      "<div class='dashboard__post'>" +
      postText.value +
      "</div>"
  );
}
